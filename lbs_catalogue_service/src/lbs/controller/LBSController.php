<?php

namespace lbs\controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use lbs\utils\Writer;

class LBSController{
  private $c;

  function __construct($c)
  {
    $this->c = $c;
  }

  public function getAllSandwichs(Request $req, Response $resp, $args){
    $type_pain = $req->getParam('type_pain');
    $page = $req->getParam('page');
    $size = $req->getParam('size');
    if(!isset($size))
      $size = 10;
    $offset = ($page - 1) * $size;

    if(isset($type_pain) && !isset($page))
      $sandwichs = \lbs\model\Sandwich::where('type_pain', 'like', $type_pain)->get();
    else if(isset($type_pain) && isset($page)){
      $sandwichs = \lbs\model\Sandwich::where('type_pain', 'like', $type_pain)->offset($offset)->limit($size)->get();
      $sandwichsCount = \lbs\model\Sandwich::where('type_pain', 'like', $type_pain)->count();
    }
    else
      $sandwichs = \lbs\model\Sandwich::get();

    $sandwichsTab = [];
    foreach ($sandwichs as $sandwich) {
      array_push($sandwichsTab, [
        'id' => $sandwich->id,
        'nom' => $sandwich->nom,
        'prix' => $sandwich->prix,
        'img' => [
          "href" => $sandwich->img
        ]
      ]);
    }

    if(!isset($page)){
      $tab = [
        "type" => "collection",
        "count" => sizeof($sandwichs),
        "sandwichs" => $sandwichsTab
      ];
    }else{
      $prevPage = (($page - 1) >= 1) ? $page - 1 : $page;
      $lastPage = $sandwichsCount / $limit;
      $nextPage = $page + 1;
      if($nextPage > $lastPage)
        $nextPage = $page;

      $tab = [
        "type" => "collection",
        "count" => $sandwichsCount,
        "size" => sizeof($sandwichs),
        "links" => [
          "next" => [
            "href" => $this->c->get('router')->pathFor('getSandwichs') . "?page={$nextPage}&size={$limit}"
          ],
          "prev" => [
            "href" => $this->c->get('router')->pathFor('getSandwichs') . "?page={$prevPage}&size={$limit}"
          ],
          "last" => [
            "href" => $this->c->get('router')->pathFor('getSandwichs') . "?page={$lastPage}&size={$limit}"
          ],
          "first" => [
            "href" => $this->c->get('router')->pathFor('getSandwichs') . "?page=1&size={$limit}"
          ]
        ],
        "sandwichs" => $sandwichsTab
      ];
    }

    return Writer::json_output($resp, 200, $tab);
  }

  public function getSandwich(Request $req, Response $resp, $args){
    $id = $args['id'];

    try{
      $sandwich = \lbs\model\Sandwich::where('id', 'like', $id)->with('categories')->firstOrFail();
      $categories = [];
      foreach ($sandwich->categories as $categorie) {
        array_push($categories, [
          "id" => $categorie->id,
          "nom" => $categorie->nom
        ]);
      }
      $tab = [
        "type" => "ressource",
        "locale" => "fr-FR",
        "links" => [
          "self" => [
            "href" => $this->c->get('router')->pathFor('getSandwichByID', ['id' => $id])
          ],
          "categories" => [
            "href" => $this->c->get('router')->pathFor('getCategoriesBySandwichID', ['id' => $id])
          ]
        ],
        "sandwich" => [
          'id' => $sandwich->id,
          'nom' => $sandwich->nom,
          'description' => $sandwich->description,
          'type_pain' => $sandwich->type_pain,
          'prix' => $sandwich->prix,
          'img' => [
            "href" => $sandwich->img
          ],
          'categories' => $categories
        ]
      ];
      return Writer::json_output($resp, 200, $tab);
    }
    catch(ModelNotFoundException $e){
      ($this->c->get('logger.error')->error("sandwich $id not found with uri ({$req->getUri()})", [404]));//<--- peut être supprimer
      return Writer::json_error($resp, 404, "sandwich $id not found");
    }
  }

  public function getCategoriesBySandwichID(Request $req, Response $resp, $args){
    $id = $args['id'];

    try{
      $sandwich = \lbs\model\Sandwich::where('id', 'like', $id)->with('categories')->firstOrFail();
      $categories = [];
      foreach ($sandwich->categories as $categorie) {
        array_push($categories, [
          "categorie" => [
            "id" => $categorie->id,
            "nom" => $categorie->nom,
            "description" => $categorie->description
          ],
          "links" => [
            "self" => [
              "href" => $this->c->get('router')->pathFor('getCategorieByID', ['id' => $categorie->id])
            ]
          ]
        ]);
      }
      $tab = [
        "type" => "ressource",
        "locale" => "fr-FR",
        "sandwich" => [
          'id' => $sandwich->id,
          'nom' => $sandwich->nom,
          'description' => $sandwich->description,
          'type_pain' => $sandwich->type_pain,
          'prix' => $sandwich->prix,
          'img' => [
            "href" => $sandwich->img
          ],
          "categories" => $categories
        ]
      ];
      return Writer::json_output($resp, 200, $tab);
    }
    catch(ModelNotFoundException $e){
      ($this->c->get('logger.error')->error("command $id not found with uri ({$req->getUri()})", [404]));//<--- peut être supprimer
      return Writer::json_error($resp, 404, "sandwich $id not found");
    }
  }

  public function getCategorieByID(Request $req, Response $resp, $args){
    $id = $args['id'];

    try{
      $categorie = \lbs\model\Categorie::where('id', 'like', $id)->firstOrFail();

      $tab = [
        "type" => "ressource",
        "date" => date("d/m/y"),
        "categorie" => [
          'id' => $categorie->id,
          'nom' => $categorie->nom,
          'description' => $categorie->description
        ],
        "links" => [
          "sandwichs" => [
            "href" => $this->c->get('router')->pathFor('getSandwichsByCategorieID', ['id' => $id])
          ],
          "self" => [
            "href" => $this->c->get('router')->pathFor('getCategorieByID', ['id' => $id])
          ]
        ]
      ];
      return Writer::json_output($resp, 200, $tab);
    }
    catch(ModelNotFoundException $e){
      ($this->c->get('logger.error')->error("categorie $id not found with uri ({$req->getUri()})", [404]));//<--- peut être supprimer
      return Writer::json_error($resp, 404, "categorie $id not found");
    }
  }

  public function getSandwichsByCategorieID(Request $req, Response $resp, $args){
    $id = $args['id'];

    try{
      $categorie = \lbs\model\Categorie::where('id', 'like', $id)->with('sandwichs')->firstOrFail();
      $sandwichs = [];
      foreach ($categorie->sandwichs as $sandwich) {
        array_push($sandwichs, [
          "sandwich" => [
            'id' => $sandwich->id,
            'nom' => $sandwich->nom,
            'description' => $sandwich->description,
            'type_pain' => $sandwich->type_pain,
            'prix' => $sandwich->prix
          ],
          "links" => [
            "self" => [
              "href" => $this->c->get('router')->pathFor('getSandwichByID', ['id' => $sandwich->id])
            ]
          ]
        ]);
      }
      $tab = [
        "type" => "ressource",
        "locale" => "fr-FR",
        "categorie" => [
          'id' => $categorie->id,
          'nom' => $categorie->nom,
          'description' => $categorie->description,
          "sandwichs" => $sandwichs
        ]
      ];
      return Writer::json_output($resp, 200, $tab);
    }
    catch(ModelNotFoundException $e){
      ($this->c->get('logger.error')->error("categorie $id not found with uri ({$req->getUri()})", [404]));//<--- peut être supprimer
      return Writer::json_error($resp, 404, "categorie $id not found");
    }

  }
}
