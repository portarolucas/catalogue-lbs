<?php

namespace lbs\utils;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Writer
{
  public static function json_output(Response $resp, int $code, array $data){
    $resp = $resp->withHeader( 'Content-Type', "application/json;charset=utf-8" )->withStatus($code);
    $json = json_encode($data);
    $resp->getBody()->write($json);
    return $resp;
  }

  public static function json_error(Response $resp, int $code, string $message, string $trace = null, string $file = null){
    $resp = $resp->withHeader( 'Content-Type', "application/json;charset=utf-8" )->withStatus($code);
    $data = [
      "type" => "error",
      "error" => $code,
      "message" => $message
    ];
    if($trace)
      $data["trace"] = $trace;
    if($file)
      $data["file"] = $file;
    $json = json_encode($data);
    $resp->getBody()->write($json);
    return $resp;
  }
}
