<?php

namespace lbs\model;

class Sandwich extends \Illuminate\Database\Eloquent\Model{
    protected $table      = 'sandwich';
    protected $primaryKey = 'id';
    public    $incrementing = false;
    public    $timestamps = false;

    public function Categories(){
      return $this->belongsToMany('lbs\model\Categorie','sand2cat','sand_id','cat_id');
    }
}
