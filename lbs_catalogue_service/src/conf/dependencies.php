<?php

return [
  'logger.error' => function(\Slim\Container $c){
    $log = new \Monolog\Logger($c->settings['debug.name']);
    $log->pushHandler(new \Monolog\Handler\StreamHandler($c->settings['error.log'], $c->settings['error.level']));
    return $log;
  }
];
