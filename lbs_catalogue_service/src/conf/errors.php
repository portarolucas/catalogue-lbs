<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use lbs\utils\Writer;

return [
  'notFoundHandler'=> function(\Slim\Container $c){
    return function(Request $req, Response $resp) use($c) : Response{
      $url = $req->getUri();
      $c['logger.error']->error("notFoundHandler :: malformed uri ($url) : request not recognized", [400]);

      return Writer::json_error($resp, 400, "malformed uri ($url) : request not recognized");
    };
  },
  'notAllowedHandler'=> function(\Slim\Container $c){
    return function(Request $req, Response $resp) use($c) : Response{
      $url = $req->getUri();
      $method = $req->getMethod();
      $c['logger.error']->error("notAllowedHandler :: impossible to use method $method with this uri ($url)", [405]);

      return Writer::json_error($resp, 405, "impossible to use method $method with this uri ($url)");
    };
  },
  'phpErrorHandler'=> function(\Slim\Container $c){
    return function(Request $req, Response $resp, \Throwable $error) use($c) : Response{
      $url = $req->getUri();
      $c['logger.error']->error("phpErrorHandler :: internal server error with this uri ($url) || {$error->getMessage()} || " . $error->getFile() . " line: " . $error->getLine(), [500]);

      return Writer::json_error($resp, 500, "internal server error: {$error->getMessage()}", $error->getTraceAsString(), $error->getFile() . " line: " . $error->getLine());
    };
  }
];
