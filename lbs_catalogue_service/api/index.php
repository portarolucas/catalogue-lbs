<?php

require_once  __DIR__ . '/../src/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request ;
use \Psr\Http\Message\ResponseInterface as Response ;

$conf_db = parse_ini_file(__DIR__ . "/../src/conf/config.ini");

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection( $conf_db ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* rendre la connexion visible dans tout le projet */
$db->bootEloquent();           /* établir la connexion */

$configuration = [
  'settings' => [
    'displayErrorDetails' => true,
    'debug.name' => 'ErrorLogger',
    'error.log' => '../src/log/error.log',
    'error.level' => \Monolog\Logger::ERROR
  ],
];
$dependencies = require_once __DIR__ . '/../src/conf/dependencies.php';
$errors = require_once __DIR__ . '/../src/conf/errors.php';

$c = new \Slim\Container(array_merge($configuration, $errors, $dependencies));
$app = new \Slim\App($c);

$app->get('/sandwichs/{id}[/]', \lbs\controller\LBSController::class . ":getSandwich")->setName('getSandwichByID');
$app->get('/sandwichs/{id}/categories', \lbs\controller\LBSController::class . ":getCategoriesBySandwichID")->setName('getCategoriesBySandwichID');
$app->get('/sandwichs[/]', \lbs\controller\LBSController::class . ":getAllSandwichs")->setName('getSandwichs');
$app->get('/categories/{id}[/]', \lbs\controller\LBSController::class . ":getCategorieByID")->setName('getCategorieByID');
$app->get('/categories/{id}/sandwichs[/]', \lbs\controller\LBSController::class . ":getSandwichsByCategorieID")->setName('getSandwichsByCategorieID');

//$app->get('/commandes[/]', \src\controller\CommandController::class . ":getAllCommands");
//$app->post('/commande/create[/]', \src\controller\CommandController::class . ":createCommand");

$app->run();

//echo "catalogue service index";
